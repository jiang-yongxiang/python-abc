import dlib
from PIL import Image
import numpy as np
import os
import cv2
from imutils import face_utils

detector = dlib.get_frontal_face_detector()

path_pre = "./model/shape_predictor_68_face_landmarks.dat"
pre = dlib.shape_predictor(path_pre)

path_model = "./model/dlib_face_recognition_resnet_model_v1.dat"
model = dlib.face_recognition_model_v1(path_model)

path_know = "./people_i_know"

path_unknow = "./face_images/guys.jpeg"

know_list = {}


def Eu(a, b):
    return np.linalg.norm(np.array(a) - np.array(b), ord=2)


for path in os.listdir(path_know):  # 建立已知人脸的字典库
    img = np.array(Image.open(path_know + "/" + path))
    name = path.split('.')[0]
    det_img = detector(img, 0)
    shape = pre(img, det_img[0])
    know_encode = model.compute_face_descriptor(img, shape)
    know_list[name] = know_encode

frame = cv2.imread(path_unknow)
# frame = frame[:, :, ::-1]
img = np.array(frame)

det_img = detector(img, 0)  # 得到检测框

for rect in enumerate(det_img):
    shape = pre(img, rect[1])
    (x, y, w, h) = face_utils.rect_to_bb(rect[1])  # rect由tuple转成bb
    unknow_encode = model.compute_face_descriptor(img, shape)
    for key, sco in know_list.items():
        if Eu(unknow_encode, sco) < 0.6:
            cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255), 3)  # 绘制检测框
            cv2.putText(img, key, (x, y + h), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)  # 绘制检测后的姓名
            break

cv2.namedWindow('image', cv2.WINDOW_NORMAL)
cv2.imshow('output', img)
cv2.waitKey(0)
