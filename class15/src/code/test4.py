import dlib
from PIL import Image
import numpy as np
import os

detector = dlib.get_frontal_face_detector()

path_pre = "./model/shape_predictor_68_face_landmarks.dat"  # 68点模型
pre = dlib.shape_predictor(path_pre)

path_model = "./model/dlib_face_recognition_resnet_model_v1.dat"  # resent模型
model = dlib.face_recognition_model_v1(path_model)

path_know = "./people_i_know"  # 已知人脸文件夹
path_unknow = "./unknow_people"  # 需要检测的人来你文件夹

know_list = {}


def Eu(a, b):  # 距离函数
    return np.linalg.norm(np.array(a) - np.array(b), ord=2)


for path in os.listdir(path_know):  # 载入已知人的名字和面部特征
    img = np.array(Image.open(path_know + "/" + path))
    name = path.split('.')[0]
    det_img = detector(img, 0)
    shape = pre(img, det_img[0])
    know_encode = model.compute_face_descriptor(img, shape)
    know_list[name] = know_encode

match = {}
for path in os.listdir(path_unknow):  # 逐个载入待检测人的面部特征
    img = np.array(Image.open(path_unknow + "/" + path))
    name = path.split('.')[0]
    det_img = detector(img, 0)
    shape = pre(img, det_img[0])
    unknow_encode = model.compute_face_descriptor(img, shape)
    match[name] = "unknow"

    for key, sco in know_list.items():  # 与已知的面部特征匹配
        if Eu(unknow_encode, sco) < 0.6:
            match[name] = key
            break

for key, value in match.items():
    print("picture:" + key + ' is ' + value)
